#!/bin/bash

DIR=$(dirname $(readlink -f $0))
ROOT=$(readlink -f $DIR/..)

# Create a brand new mkdocs project
rm -rf theme
mkdocs new theme
cd theme

# Ensure these directories exist
mkdir -p {css,js,fonts}

# Build and clean (remove files not related to theme)
mkdocs build
rm -rf docs mkdocs.yml
mv site/* .
rm -rf site
rm -rf img
rm -rf __init__.py
rm -rf __init__.pyc
rm -rf 404.html
rm -rf index.html
rm -rf sitemap.xml

# Remove default bootstrap and font-awesome
rm css/bootstrap* css/font-awesome* js/bootstrap*

# Update bootstrap
git clone --depth=1 https://github.com/twbs/bootstrap.git tmp-bootstrap
mv tmp-bootstrap/dist/js/bootstrap.min.js js
cp tmp-bootstrap/dist/fonts/* fonts/
wget https://bootswatch.com/flatly/bootstrap.min.css -P css
rm -rf tmp-bootstrap

# Update font-awesome
git clone --depth=1 https://github.com/FortAwesome/Font-Awesome.git tmp-font-awesome
cp tmp-font-awesome/css/font-awesome.min.css css
cp tmp-font-awesome/fonts/* fonts/
rm -rf tmp-font-awesome

# Update base.html so that it includes the downloaded bootstrap, font-awesome, and jquery files
sed -i 's/bootstrap-.*.min.css/bootstrap.min.css/g' base.html
sed -i 's/bootstrap-.*.min.js/bootstrap.min.js/g' base.html
sed -i 's/font-awesome-.*.css/font-awesome.min.css/g' base.html

# Move extra_javascript include lines to a point before 
# search.js and require.js so that lightbox.js works
echo "Fixing order of javascript inclusion..."
sed -n '1,64p'  base.html >> tmp-base.html
sed -n '67,69p' base.html >> tmp-base.html
sed -n '65,66p' base.html >> tmp-base.html
sed -n '70,$p'  base.html >> tmp-base.html
mv tmp-base.html base.html
