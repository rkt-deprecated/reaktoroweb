# Reaktoro

Reaktoro is a unified framework for modeling chemically reactive systems. It provides methods for chemical equilibrium and kinetic calculations for multiphase systems. Reaktoro is mainly developed in C++ for performance reasons. A Python interface is available for a more convenient and simpler use of the scientific library. Currently Reaktoro can interface with two widely used geochemical software: [PHREEQC](http://wwwbrr.cr.usgs.gov/projects/GWC_coupled/phreeqc/) and [GEMS](http://gems.web.psi.ch/). 

Check the [installation](../installation/#installation) instructions, and the [tutorial page](../tutorial/#tutorial) to get stated. 

Reaktoro is developed by Dr. Allan Leal at ETH Zurich, in the [Geothermal Energy & Geofluids Group][geg] led by Prof. Martin Saar. 

This website is still in development. Please address any suggestions and/or corrections to [allan.leal@erdw.ethz.ch](mailto:allan.leal@erdw.ethz.ch).

[geg]:https://geg.ethz.ch/